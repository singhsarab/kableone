//
//  ViewController.swift
//  kableone
//
//  Created by chdsez on 2023-03-31.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var firstPlaylistCollectionView: UICollectionView!
    @IBOutlet weak var sagaHitsCollectionView: UICollectionView!
    
    var channels: [String] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.backgroundColor = .clear
        parseJson();
    }
    
    private func parseJson() {
        guard let path = Bundle.main.path(forResource: "data", ofType: ".json") else {
            return
        }
        let url = URL(fileURLWithPath: path);
        do {
            let jsonData = try Data(contentsOf: url)
            let json = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
            let data = MyData(json: json as! [String : Any])
            channels = data?.channels ?? []
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView === firstPlaylistCollectionView) {
            return 1
        }
        return channels.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyCollectionCell
        cell.layer.cornerRadius = 5;
        if (channels.count != 0 && collectionView == sagaHitsCollectionView) {
            cell.imageView.image = UIImage(named: channels[indexPath.row])
        }
        return cell
    }
}
