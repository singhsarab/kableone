//
//  Model.swift
//  kableone
//
//  Created by chdsez on 2023-04-02.
//

import Foundation

struct Result: Codable {
    let data: [ResultItem];
}

struct ResultItem: Codable {
    let channels: [String]
}

class MyData {
    let channels: [String]
    
    init?(json: [String: Any]) {
        guard let dataArray = json["data"] as? [[String: Any]], // get the data array
              let dataDict = dataArray.first, // get the first (and only) dictionary in the array
              let channelsArray = dataDict["channels"] as? [String] // get the channels array from the dictionary
        else {
            return nil // return nil if parsing fails
        }
        
        self.channels = channelsArray // set the channels property
    }
}
