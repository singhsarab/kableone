//
//  MyCollectionCell.swift
//  kableone
//
//  Created by chdsez on 2023-04-01.
//

import UIKit

class MyCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
