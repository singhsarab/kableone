//
//  MyTabBarViewController.swift
//  kableone
//
//  Created by chdsez on 2023-04-03.
//

import UIKit

class MyTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBar.scrollEdgeAppearance?.backgroundColor = .clear
        self.tabBar.scrollEdgeAppearance?.backgroundImage = UIImage()
        self.tabBar.scrollEdgeAppearance?.shadowImage = UIImage()
        self.tabBar.standardAppearance.backgroundImage = UIImage()
        self.tabBar.standardAppearance.shadowImage = UIImage()
        self.tabBar.clipsToBounds = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
